package com.bitpartner.allspark;

public class Constant {

    public static String CERT_KEY = "___ALL__SPARK___";
    public static String EVENT_URL = "http://bitpartner.co.kr/ico/allspark/recom/event";

    public static String IS_USED_FAILED = "이미 사용중인 유저입니다!";
    public static String IS_USED_SUCCESS = "사용 가능합니다.";

    public static boolean IS_USED_SUCCESS_CODE = true;
    public static boolean IS_USED_FAILED_CODE = false;



}
